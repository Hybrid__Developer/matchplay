import React, { Component } from 'react';
import { View, ImageBackground, Text, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../utility/index';
import LinearGradient from 'react-native-linear-gradient';


import styles from '../../app/screens/Home/style'
export default class MainHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            MainHeader: '',
         
        }
    }
  render() {
    return (
      <View>
         <ScrollView>

<View style={{flexDirection:"row",backgroundColor:"white",justifyContent:"space-between" ,width: wp("100%"), height: hp("10%"),}}>

<Image

source={require('../assets/menuicon.png')}
style={{ width: wp("8%"), height: hp("5%"),alignSelf:"center",justifyContent:"center",marginLeft:"4%"}}></Image>
<View style={{justifyContent:"center",flexDirection:"row",alignItems:"center",marginLeft:"19%"}}>
  <Text style={{fontWeight:"bold",color:"#2B7225",fontSize:21,textAlign:"center",fontStyle:"italic"}}>MP</Text>
  <Text style={{fontWeight:"bold",color:"#000000",fontSize:19,textAlign:"center",marginLeft:"3%"}}>MatchPlay!</Text>
  </View>

<Image

source={require('../assets/wallet.png')}
style={{ width: wp("7%"), height: hp("3%"),justifyContent:"center",alignSelf:"center",marginLeft:"11%",marginRight:"5%"}}></Image>

<Image

source={require('../assets/bell.png')}
style={{ width: wp("5%"), height: hp("2.9%"),justifyContent:"center",alignSelf:"center",marginRight:"4%"}}></Image>
</View>

</ScrollView>
      </View>

    );
  }
}


