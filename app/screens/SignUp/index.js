import React, { Component } from 'react';
import { View, ImageBackground, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
import Button from "../../constants/button"
import TextInputs from "../../constants/TextInput"
import styles from './style';
import Logo from "../../constants/Logo"

export default class SignIn extends Component {


    render() {
        return (
            <View>

                <ImageBackground source={require('../../assets/background.png')} style={styles.background}>
                            <Logo></Logo>
                        <View>
                            <View style={{ marginTop: "7%" }}>
                                <TextInputs
                                    placeHolder={"E-MAIL"}
                                    uri={require("../../assets/email.png")}
                                    image={styles.emailimg}
                                ></TextInputs>
                            </View>
                            <View style={{ marginTop: "7%" }}>

                                <TextInputs
                                    placeHolder={"USERNAME"}
                                    uri={require("../../assets/user.png")}
                                    image={styles.userimg}

                                ></TextInputs>
                            </View>
                            <View style={{ marginTop: "7%" }}>

                                <TextInputs
                                    placeHolder={"PASSWORD"}
                                    uri={require("../../assets/lock.png")}
                                    image={styles.lockimg}
                                ></TextInputs>
                            </View>
                            <View style={{ marginTop: "7%" }}>

                                <TextInputs
                                    placeHolder={"CONFIRM PASSWORD"}
                                    uri={require("../../assets/lock.png")}
                                    image={styles.pswdimg}
                                ></TextInputs>
                            </View>

                            <View style={{marginTop:50}}>
                                <Button title={"SIGN UP"} ></Button>
                                </View>
                            <View style={styles.linkview}>
                                <Text style={styles.linktext}>ALREADY A PLAYER! GOOD. TEE-OFF!</Text>
                            </View>
                        </View>
                </ImageBackground>

            </View>
        );
    }
}

