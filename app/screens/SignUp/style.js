import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
// import * as colors from '../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({
    background:
    {
        width: wp("100%"),
        height: hp("100%")
    },
    mptext:
    {
        color: "#fff",
        fontSize: 90,
        fontWeight: "bold",
        marginTop: "10%",
        fontStyle: "italic",
        textAlign: "center",
        letterSpacing: 3
    },

    line:
    {
        justifyContent: "center",
        marginTop: -20,
        marginLeft: "31%",
        width: wp("43%"),
        height: hp("0.7%")
    },
    text:
    {
        color: "#fff",
        fontSize: 25,
        fontWeight: "bold",
        marginLeft: "30%",
        letterSpacing: 3
    },
    line2:
    {
        justifyContent: "center",
        marginTop: 3,
        marginLeft: "31%",
        width: wp("43%"),
        height: hp("0.7%")
    },
    linkview:
    {
        marginTop: "3%",
        marginBottom: "22%"
    },
    linktext:
    {
        textAlign: "center",
        color: "white",
        fontWeight: "800",
        fontSize: 15,
        textDecorationLine: "underline"
    },
    emailimg :{ 
                                    
        ...Platform.select({
            ios: {
                width: wp("6.5%"), height: hp("2.5%"), 
        
            },
            android: {
                width: wp("7%"), height: hp("2.8%"), 
            }
        
          }),
        
        justifyContent: "center", alignSelf: "center", alignItems: "flex-start", marginLeft: "5%" },
        userimg:
        {



            ...Platform.select({
                ios: {
                    width: wp("6%"),
                    height: hp("3%"),
            
                },
                android: {
                    width: wp("7%"),
                    height: hp("4%"),
                }
            
              }),

            justifyContent: "center",
            alignSelf: "center",
            alignItems: "flex-start",
            marginLeft: "5%"
        },

        lockimg:
        {



            ...Platform.select({
                ios: {
                    width: wp("6%"),
                    height: hp("3%"),
            
                },
                android: {
                    width: wp("7%"),
                    height: hp("4%"),
            
                }
            
              }),




          
            justifyContent: "center",
            alignSelf: "center",
            alignItems: "flex-start",
            marginLeft: "5%"
        },

        pswdimg:
        {



            ...Platform.select({
                ios: {
                    width: wp("6%"),
                    height: hp("3%"),
            
                },
                android: {
                    width: wp("7%"),
                    height: hp("4%"),
            
                }
            
              }),




          
            justifyContent: "center",
            alignSelf: "center",
            alignItems: "flex-start",
            marginLeft: "5%"
        },
});

export default styles;