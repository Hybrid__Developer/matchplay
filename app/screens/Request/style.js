import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({

    background:
    {
        width: wp("100%"),
        height: hp("100%"),
        
    },
    mainview:
    {
        backgroundColor: "rgba(255, 255, 255, 0.4)",
        width: '90%',
        height: hp("20%"),
        borderRadius: 15,
        alignSelf: "center",
        marginTop: 30
    },
    nextimg:
    {
        width: wp("4.8%"),
        height: hp("5%"),
        alignSelf:"flex-end",
        marginTop:-55,
        marginRight:"9%"
        
        
    },
  loc:
  {
      width:wp("17%"),
      height: hp("10%"),
    //   justifyContent:"center",
      alignSelf:"center",marginTop:10
  }




  
});

export default styles;