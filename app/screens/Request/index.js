import React, { Component } from 'react';
import { View, ImageBackground, Text, Image, StyleSheet } from 'react-native';
import MainHeader from '../../Components/MainHeader';
import styles from './style';
export default class Request extends Component {
    render() {
        return (
            <View>


                <MainHeader></MainHeader>


                <ImageBackground style={styles.background} source={require('../../assets/background.png')}>

                    <View>

                        <View style={styles.mainview}>
                            <Image

                                source={require('../../assets/location.png')}
                                style={styles.loc}
                                resizeMode="cover"></Image>
                                <Text style={{color:"white",fontWeight:"bold",fontSize:15,textAlign:"center"}}>NEARBY</Text>

                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}