import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
// import * as colors from '../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({
    background:
    {
        width: "100%",
        height: "100%"
    },
    mpTxt: {
        color: "#fff",
        fontSize: 90,
        fontWeight: "bold",
        marginTop: "10%",
        fontStyle: "italic",
        textAlign: "center",
        letterSpacing: 3
    },
    line:
    {
        justifyContent: "center",
        marginTop: -22,
        marginLeft: "31%",
        width: wp("43%"),
        height: hp("0.7%")
    },
    txt:
    {
        color: "#fff",
        fontSize: 25,
        fontWeight: "bold",
        marginLeft: "30%",
        letterSpacing: 3
    },
    line2:
    {
        justifyContent: "center",
        marginTop: 1,
        marginLeft: "31%",
        width: wp("43%"),
        height: hp("0.7%")
    },
    frgttxt:
    {
        color: "white",
        fontWeight: "bold",
        fontSize: 18,
        textAlign: "center"
    },
    textotp:
    {
        color: "white",
        fontSize: 16,
        textAlign: "center",
        paddingLeft: "5%",
        paddingRight: "5%"
    },
    chngpsw:
    {
        color: "white",
        fontSize: 16,
        textAlign: "center",
        paddingLeft: "10%",
        paddingRight: "10%"
    },
    row:
    {
        flexDirection: "row",
        justifyContent: "center",
        alignSelf: "center",
      


...Platform.select({
    ios: {
        marginTop: "6%",
        marginBottom:"3%",


    },
    android: {
        marginTop: "3%",
        marginBottom:"3%",


    }

  }),
    },
    eline:
    {
        width: wp("35%"),
        height: hp("0.3%"),
        justifyContent: "center",
        alignSelf: "center",
        marginRight: "3%"
    },
    or:
    {
        color: "grey",
        textAlign: "center",
        justifyContent: "center",
        alignSelf: "center",
        fontSize: 17
    },
    eline2:
    {
        width: wp("35%"),
        height: hp("0.3%"),
        justifyContent: "center",
        alignSelf: "center",
        marginLeft: "3%"
    },
    phoneview:
    {
        ...Platform.select({
            ios: {
                marginTop: "5%"

            },
            android: {
                // marginTop:"1%" 

            }

        }),
    },
    phoneimg:
    {

        ...Platform.select({
            ios: {
                width: wp("6%"),
                height: hp("3%"),

            },
            android: {
                width: wp("8%"),
                height: hp("4.3%"),

            }

        }),
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "flex-start",
        marginLeft: "5%"
    },
    emailimg:
    {

        ...Platform.select({
            ios: {
                width: wp("6.5%"), height: hp("2.5%"),

            },
            android: {
                width: wp("7%"), height: hp("2.8%"),
            }

        }),

        justifyContent: "center", alignSelf: "center", alignItems: "flex-start", marginLeft: "5%"
    },
    btnview:
    {

        ...Platform.select({
            ios: {
                marginTop: 60

            },
            android: {
                marginTop: 45

            }

        }),
    }


});

export default styles;