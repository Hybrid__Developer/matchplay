import React, { Component } from 'react';
import { View, ImageBackground, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
import Button from "../../constants/button"
import TextInputs from "../../constants/TextInput"
import styles from './style';
import Logo from "../../constants/Logo";
export default class SignIn extends Component {


    render() {
        return (
            <View>

                <ImageBackground source={require('../../assets/background.png')} style={styles.background}>
                    <Logo></Logo>
                    <View>
                        <View style={{ marginTop: "5%" }}>
                            <Text style={styles.frgttxt}>Forgot Password</Text>
                        </View>
                        <View style={{ marginTop: "3%" }}>
                            <Text style={styles.textotp}>Enter Your e-mail to receive OTP in Your</Text>
                        </View>
                        <View style={{ marginTop: "1%" }}>
                            <Text style={styles.chngpsw}>e-mail to change your password</Text>
                        </View>

                        <View style={{ marginTop: "7%" }}>

                            <TextInputs
                                placeHolder={"E-MAIL"}
                                uri={require("../../assets/email.png")}
                                image={styles.emailimg}
                            ></TextInputs>
                        </View>


                        <View style={styles.row}>
                            <Image source={require('../../assets/OrLine.png')}
                                style={styles.eline}
                                resizeMode="cover">

                            </Image>
                            <Text style={styles.or}>OR</Text>
                            <Image source={require('../../assets/Line.png')}
                                style={styles.eline2}
                                resizeMode="cover">

                            </Image>
                        </View>

                        <View style={styles.phoneview}>

                            <TextInputs
                                placeHolder={"PHONE NUMBER"}
                                keyboard={"numeric"}
                                uri={require("../../assets/phone.png")}
                                image={styles.phoneimg}

                            ></TextInputs>
                        </View>
                        <View style={styles.btnview}>
                            <Button title={"SUBMIT"} navigate={() => this.props.navigation.navigate("OtpVerification")} ></Button>

                        </View>
                    </View>

                </ImageBackground>
            </View>
        );
    }
}

