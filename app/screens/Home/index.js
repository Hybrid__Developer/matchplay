import React, { Component } from 'react';
import { View, ImageBackground, Text, Image, StyleSheet } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
import LinearGradient from 'react-native-linear-gradient';
import MainHeader from '../../Components/MainHeader';
import UserAvatar from 'react-native-user-avatar';

import Swiper from 'react-native-swiper'
import { HeaderTitle } from 'react-navigation-stack';
import Avatar from 'react-avatar';

import styles from './style';
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data1: [
        {
          Name1: "ALEX SANDROS",
          Name2: "Shares Match results",
          image: require('../../assets/img.png'),
          Time: "06:30 PM",
          Header: "ALEX SANDROS WINS THE MATCH!",
          image1: require('../../assets/img.png'),
          Flag1: require('../../assets/germany.png'),
          Player1: "ALEX SANDROS",
          address: "Germany",
          score: '23:18',
          club: "18 Hole Match \n At Lakeside Golf Club \n Tournament: N0",
          image2: require('../../assets/img.png'),
          Flag2: require('../../assets/germany.png'),
          Player2: "JAMES S JR",
          address2: "Germany",
          like: require('../../assets/like.png'),
          comment: require('../../assets/comment.png'),
          arrow: require('../../assets/nextarrow.png'),
          likes: "45k",
          comments: "2435",
          share: "135",

        }],

      data2: [
        {

          advertisement: require('../../assets/advertisement.png')

        }

      ],

      data3: [
        {
          Name1: "ALEX SANDROS",
          Name2: "Shares Match results",
          image: require('../../assets/img.png'),
          Time: "06:30 PM",
          Header: "ALEX SANDROS WINS THE MATCH!",
          image1: require('../../assets/img.png'),
          Flag1: require('../../assets/germany.png'),
          Player1: "ALEX SANDROS",
          address: "Germany",
          score: '23:18',
          club: "18 Hole Match \n At Lakeside Golf Club \n Tournament: N0",
          image2: require('../../assets/img.png'),
          Flag2: require('../../assets/germany.png'),
          Player2: "JAMES S JR",
          address2: "Germany",
          like: require('../../assets/like.png'),
          comment: require('../../assets/comment.png'),
          arrow: require('../../assets/nextarrow.png'),
          likes: "45k",
          comments: "2435",
          share: "135",

        },
        {
          Name1: "ALEX SANDROS",
          Name2: "Shares Match results",
          image: require('../../assets/img.png'),
          Time: "06:30 PM",
          Header: "ALEX SANDROS WINS THE MATCH!",
          image1: require('../../assets/img.png'),
          Flag1: require('../../assets/germany.png'),
          Player1: "ALEX SANDROS",
          address: "Germany",
          score: '23:18',
          club: "18 Hole Match \n At Lakeside Golf Club \n Tournament: N0",
          image2: require('../../assets/img.png'),
          Flag2: require('../../assets/germany.png'),
          Player2: "JAMES S JR",
          address2: "Germany",
          like: require('../../assets/like.png'),
          comment: require('../../assets/comment.png'),
          arrow: require('../../assets/nextarrow.png'),
          likes: "45k",
          comments: "2435",
          share: "135",

        }],


    }

  }
  render() {
    return (
      <View>
        <MainHeader></MainHeader>


        <ImageBackground style={styles.background} source={require('../../assets/background.png')}>
        <ScrollView>
          <View style={{ flex: 1,marginBottom:"50%"}}>

            <View>
                <Swiper style={styles.wrapper} showsButtons={true}
                  prevButton={
                    <Image
                      source={require('../../assets/preview.png')}
                      style={styles.previmg}
                      resizeMode="cover"></Image>
                  }
                  nextButton={
                    <Image
                      source={require('../../assets/next.png')}
                      style={styles.nextimg}
                      resizeMode="cover"></Image>
                  }

                  showsPagination={false}>

                  <View>
                    <Image

                      source={require('../../assets/golf.png')}
                      style={styles.golfimg}
                      resizeMode="cover"></Image>
                  </View>
                  <View>
                    <Image

                      source={require('../../assets/golf.png')}
                      style={styles.golfimg}
                      resizeMode="cover"></Image>
                  </View>

                </Swiper>


                <View>
                  {this.state.data1.map((element => {
                    return (
                      <View style={styles.row1}>
                        <View>

                          <View style={styles.row11}>
                            <View>
                              <Image style={styles.img1} source={element.image} />

                            </View>
                            <View style={styles.columnview1}>
                              <Text style={styles.text1}>{element.Name1}</Text>
                              <Text style={styles.undertext1}>{element.Name2}</Text>
                            </View>
                            <View>
                              <Text style={styles.time}>{element.Time}</Text>
                            </View>
                          </View>

                        </View>

                        <View style={styles.mainview}>
                          <View>
                            <View>
                              <Text style={styles.heading}>{element.Header}</Text>
                            </View>


                            <View style={styles.row2}>
                              <View>

                                <View style={styles.imginner}>


                                  <Image

                                    source={element.image1}
                                    style={styles.img}
                                    resizeMode="cover"></Image>

                                  <View style={{ flexDirection: "column" }}>
                                    <Text style={styles.txtcl1}>{element.Player1}</Text>
                                    <View style={styles.rowimggermany}>
                                      <Image

                                        source={element.Flag1}
                                        style={styles.flag}
                                        resizeMode="cover"></Image>

                                      <Text style={styles.germany}>{element.address}</Text>
                                    </View>
                                  </View>

                                </View>
                              </View>

                              <View>

                                <View style={styles.clm2view}>
                                  <Text style={styles.text}>{element.score}</Text>
                                  <Text style={styles.txtline}> {element.club}</Text>
                                </View>

                              </View>
                              <View>
                                <View style={styles.clm3view}>
                                  <Image

                                    source={element.image2}
                                    style={styles.imgclm3}
                                    resizeMode="cover"></Image>

                                  <View style={styles.viewjames}>
                                    <Text style={styles.txtcl1}>{element.Player2}</Text>
                                    <View style={{ flexDirection: "row", }}>
                                      <Image

                                        source={element.Flag2}
                                        style={styles.flag}
                                        resizeMode="cover"></Image>

                                      <Text style={styles.germany}>{element.address}</Text>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>

                        </View>




                        <View style={styles.viewlike}>


                          <View>

                            <Image

                              source={element.like}
                              style={styles.likeimg}
                              resizeMode="cover"></Image>
                          </View>
                          <Text style={styles.likes}>{element.likes}</Text>

                          <View>
                            <Image

                              source={element.comment}
                              style={styles.commentimg}
                              resizeMode="cover"></Image>
                          </View>
                          <Text style={styles.commenting}>{element.comments}</Text>
                          <View>
                            <Image

                              source={element.arrow}
                              style={styles.arrowimg}
                              resizeMode="cover"></Image>
                          </View>
                          <Text style={styles.shares}>{element.share}</Text>

                        </View>
                        <View style={styles.rowline}>
                  <View style={styles.line} />

                </View>
                      </View>


                    )
                  }))}
                </View>
               

                <View >
                  {this.state.data2.map((element => {
                    return (
                      <View style={{ 
                        marginBottom:"15%"
                      }}>
                        <Image

                          source={element.advertisement}
                          style={styles.add}
                          resizeMode="stretch"></Image>
                        <View style={styles.addtxtview}>



                          <TextInput
                            require
                            placeholder="Advertisement.*"
                            placeholderTextColor="black"

                            style={styles.txt}
                          />
                        </View>


                      </View>

                    )
                  }))}
                  <View>
                    {this.state.data3.map((element => {
                      return (
                        <View style={styles.row1}>
                          <View>

                            <View style={styles.row11}>
                              <View>
                                <Image style={styles.img1} source={element.image} />

                              </View>
                              <View style={styles.columnview1}>
                                <Text style={styles.text1}>{element.Name1}</Text>
                                <Text style={styles.undertext1}>{element.Name2}</Text>
                              </View>
                              <View>
                                <Text style={styles.time}>{element.Time}</Text>
                              </View>
                            </View>

                          </View>

                          <View style={styles.mainview}>
                            <View>
                              <View>
                                <Text style={styles.heading}>{element.Header}</Text>
                              </View>


                              <View style={styles.row2}>
                                <View>

                                  <View style={styles.imginner}>


                                    <Image

                                      source={element.image1}
                                      style={styles.img}
                                      resizeMode="cover"></Image>

                                    <View style={{ flexDirection: "column" }}>
                                      <Text style={styles.txtcl1}>{element.Player1}</Text>
                                      <View style={styles.rowimggermany}>
                                        <Image

                                          source={element.Flag1}
                                          style={styles.flag}
                                          resizeMode="cover"></Image>

                                        <Text style={styles.germany}>{element.address}</Text>
                                      </View>
                                    </View>

                                  </View>
                                </View>

                                <View>

                                  <View style={styles.clm2view}>
                                    <Text style={styles.text}>{element.score}</Text>
                                    <Text style={styles.txtline}> {element.club}</Text>
                                  </View>

                                </View>
                                <View>
                                  <View style={styles.clm3view}>
                                    <Image

                                      source={element.image2}
                                      style={styles.imgclm3}
                                      resizeMode="cover"></Image>

                                    <View style={styles.viewjames}>
                                      <Text style={styles.txtcl1}>{element.Player2}</Text>
                                      <View style={{ flexDirection: "row", }}>
                                        <Image

                                          source={element.Flag2}
                                          style={styles.flag}
                                          resizeMode="cover"></Image>

                                        <Text style={styles.germany}>{element.address}</Text>
                                      </View>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            </View>

                          </View>




                          <View style={styles.viewlike}>


                            <View>

                              <Image

                                source={element.like}
                                style={styles.likeimg}
                                resizeMode="cover"></Image>
                            </View>
                            <Text style={styles.likes}>{element.likes}</Text>

                            <View>
                              <Image

                                source={element.comment}
                                style={styles.commentimg}
                                resizeMode="cover"></Image>
                            </View>
                            <Text style={styles.commenting}>{element.comments}</Text>
                            <View>
                              <Image

                                source={element.arrow}
                                style={styles.arrowimg}
                                resizeMode="cover"></Image>
                            </View>
                            <Text style={styles.shares}>{element.share}</Text>

                          </View>
                          <View style={styles.rowline}>
                  <View style={styles.line} />

                </View>
                        </View>


                      )
                    }))}
                  </View>
                </View>




            </View>

          </View>
          </ScrollView>

        </ImageBackground>

      </View>

    );
  }
}

