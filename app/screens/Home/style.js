import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
// import * as colors from '../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        height: hp("40%")
    },
    slide1: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "rgba(255, 255, 255, 0.6)",
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    background:
    {
        width: wp("100%"),
        height: hp("100%"),
        
    },
    previmg:
    {
        width: wp("4%"),
        height: hp("4%")
    },
    nextimg:
    {
        width: wp("4%"),
        height: hp("4%")
    },
    golfimg:
    {
        width: wp("100%"),
        height: hp("40%")
    },
    row1:
    {
        marginTop: "10%",

    },
    row11:
    {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    img1:
    {
        width: wp("22%"),
        height: hp("11%"),
        marginTop: -15
    },
    columnview1:
    {
        flexDirection: "column",
        marginLeft: "5%"
    },
    text1:
    {
        fontSize: 11,
        color: "white",
        fontWeight: "bold",
    },
    undertext1:
        { fontSize: 15, color: "white" },
    time:
    {
        fontSize: 15,
        color: "white",
        marginLeft: "23%",
        justifyContent: "flex-end",
        marginRight: "15%"
    },
    mainview:
    {
        backgroundColor: "rgba(255, 255, 255, 0.4)",
        width: '90%',
        height: hp("25%"),
        borderRadius: 15,
        alignSelf: "center",
        marginTop: 30
    },
    heading:
    {
        fontSize: 13,
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        marginTop: "5%"
    },
    row2:
    {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    imginner:
    {
        flexDirection: "column",
        marginLeft: "8%",
        justifyContent: "center",
        alignSelf: "center"
    },

    img:
    {
        width: wp("15%"),
        height: hp("10%"),
        marginLeft: "2%",
        justifyContent: "center",
        alignSelf: "center"
    },


    viewjames:
        { flexDirection: "column" },
    txtcl1:
    {
        fontSize: 11,
        color: "white",
        fontWeight: "bold"
    },

    flag:
    {
        width: wp("4.5%"),
        height: hp("2.6%"),
        marginLeft: "0%"
    },

    rowimggermany:
        { flexDirection: "row", },


    germany:
    {
        fontSize: 11,
        color: "white",
        marginLeft: "4%"
    },
    clm2view:
    {
        flexDirection: "column",
        marginTop: "16%"
    },
    text:
    {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    txtline:
    {
        color: "white",
        fontWeight: "900",
        textAlign: "center",
        letterSpacing: 1,
        fontSize: 10
    },
    clm3view:
    {
        flexDirection: "column",
        marginRight: "7%",
        justifyContent: "center",
        alignSelf: "center"
    },
    imgclm3:
    {
        width: wp("15%"),
        height: hp("10%"),
        marginLeft: "2%",
        justifyContent: "center",
        alignSelf: "center",
        marginRight: "5%"
    },
    viewlike:
    { flexDirection: "row",
     width: wp("20%"),
      alignItems: "center", 
      marginTop: "5%", 
      marginBottom: "0%",
       marginLeft: "12%" },
       likeimg:
       {
        width: wp("6%"),
        height: hp("3%"),
        marginRight: "3%"
      },
likes:
{ color: "white",
 marginRight: "15%",
  fontSize: 10 },
  commentimg:
  {
    width: wp("5.5%"),
    height: hp("3%"),
    marginLeft: "5%",

  },
  commenting:
  { color: "white",
   marginRight: "15%", 
  fontSize: 10 },
  arrowimg:
  {
    width: wp("7%"),
    height: hp("3%"),
    marginLeft: "5%",

  },
  shares:
  { color: "white", 
  marginRight: "15%", 
  fontSize: 10 },
  rowline:
  { flexDirection: 'row', 
  alignItems: 'center',
   marginTop: "5%",
    width: wp("90%"),
     marginLeft: "5%", 
  marginRight: "5%" },
  line:
  { flex: 1,
     height: 1, 
    backgroundColor: "white" },

add:
{
    top: 38,
    width: wp("100%"),
    height: hp("25%")

  },
  addtxtview:
  {
    backgroundColor: "rgba(255, 255, 255, 0.41)",
    marginTop: "-7%",
    borderRadius: 10,


    width: wp("90%"),
    height: hp('6%'),
    marginLeft: "5%",
    marginRight: "5%",
    justifyContent: "center"
  },
  txt:
  {
    color: "black",
    fontSize: 13,
    marginLeft: "6%",
    fontWeight: "900"
  },
});

export default styles;