import React, { Component } from 'react';
import { View, ImageBackground, Text, Image,  TouchableOpacity, Platform } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
import Button from "../../constants/button"
import { HeaderTitle } from 'react-navigation-stack';
import TextInputs from "../../constants/TextInput"
import styles from './style';

import { NavigationActions, StackActions } from 'react-navigation';
import Logo from "../../constants/Logo"
export default class SignIn extends Component {

goToHome=()=>{
    this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Home' })]
        })
      );
      this.props.navigation.navigate('BottomTab2');
}
    render() {
        return (
            <View>

                <ImageBackground source={require('../../assets/background.png')} style={styles.background}>
                      

                        <Logo></Logo>
                        <View>
                            
                            <View style={{ marginTop: "15%" }}>

                                <TextInputs
                                    placeHolder={"USERNAME"}
                                    uri={require("../../assets/user.png")}
                                    image={styles.userImg}
                                ></TextInputs>
                            </View>
                            <View style={styles.passwordview}>

                                <TextInputs
                                    placeHolder={"PASSWORD"}
                                    uri={require("../../assets/lock.png")}
                                    image={styles.lk}
                                ></TextInputs>
                            </View>
                                    <Text onPress={() => this.props.navigation.navigate("ForgotPassword")} style={styles.forgottext}>FORGOT PASSWORD?</Text>
                            <View style={{marginTop:50}}>

                                <Button title={"SIGN IN"} navigate={()=>this.goToHome()} ></Button>
</View>

                            <View style={styles.linkview}>
                                <Text style={styles.linktext} onPress={()=>this.props.navigation.navigate("SignUp")}>"NOT ON MATCH PLAY! WHY? SIGN UP HERE"</Text>
                            </View>
                        </View>
                </ImageBackground>
            </View>
        );
    }
}

