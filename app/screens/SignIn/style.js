import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({
    background:
    {
        width: "100%",
        height: "100%"
    },
    logotext:
    {
        color: "#fff",
        fontSize: 90,
        fontWeight: "bold",
        marginTop: "10%",
        fontStyle: "italic",
        textAlign: "center",
        letterSpacing: 3
    },
    line1:
    {
        justifyContent: "center",
        marginLeft: "4%",
        width: wp("38%"),
        height: hp("0.5%"),
        alignSelf:"center",

        ...Platform.select({
            ios: {
                marginTop: -18,

        
            },
            android: {
                marginTop: -22,

        
            }
        
          }),


    },
    textmp:
    {
        color: "#fff",
        fontSize: 20,
        fontWeight: "bold",
        // marginLeft: "30%",
        textAlign:"center",
        letterSpacing: 3
    },
    line2:
    {
        justifyContent: "center",
        marginTop: 1,
        marginLeft: "4%",
        width: wp("38%"),
        height: hp("0.5%"),
        alignSelf:"center"
    },
   
    
    forgotview:
        { },
    forgottext:
    {
        color: "white",
        justifyContent: "flex-end",
        alignSelf: "flex-end",
        marginRight: "7%",
        marginTop: 8
    },
    linkview:
    {
        marginTop: 10,
    },
    linktext:
    {
        textAlign: "center",
        color: "white",
        fontWeight: "800",
        fontSize: 13,
        textDecorationLine: "underline"
    },
    passwordview: { 

        ...Platform.select({
            ios: {
              marginTop:"13%",
        
            },
            android: {
                marginTop: "9%",
        
            }
        
          }),
        },
       lk:
       {



        ...Platform.select({
            ios: {
                width: wp("6%"),
                height: hp("3%"),
        
            },
            android: {
                width: wp("7%"),
                height: hp("4%"),
        
            }
        
          }),

        justifyContent: "center",
        alignSelf: "center",
        alignItems: "flex-start",
        marginLeft: "5%"
    },
userImg: {



        ...Platform.select({
            ios: {
                width: wp("6%"),
                height: hp("3%"),
        
            },
            android: {
                width: wp("7%"),
                height: hp("4%"),
            }
        
          }),

        justifyContent: "center",
        alignSelf: "center",
        alignItems: "flex-start",
        marginLeft: "5%"
    },
    

});

export default styles;