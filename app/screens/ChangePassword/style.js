import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
// import * as colors from '../../constants/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({
    background:
    {
        width: "100%",
        height: "100%"
    },
    logotext:
    {
        color: "#fff",
        fontSize: 90,
        fontWeight: "bold",
        marginTop: "10%",
        fontStyle: "italic",
        textAlign: "center",
        letterSpacing: 3
    },
    line1:
    {
        justifyContent: "center",
        marginTop: -22,
        marginLeft: "31%",
        width: wp("38%"),
        height: hp("0.7%")
    },
    textmp:
    {
        color: "#fff",
        fontSize: 22,
        fontWeight: "bold",
        marginLeft: "30%",
        letterSpacing: 3
    },
    line2:
    {
        justifyContent: "center",
        marginTop: 1,
        marginLeft: "31%",
        width: wp("38%"),
        height: hp("0.7%")
    },
    viewtxt  :{ marginTop: "7%" },
   
    txt:
    {
        color: "white",
        fontWeight: "bold",
        fontSize: 21,
        textAlign: "center"
    },
    viewlock   :
    { marginTop: "9%" },
    imglock:
    {
        width: wp("7%"),
        height: hp("4%"),
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "flex-start",
        marginLeft: "5%"
    }
    
});

export default styles;