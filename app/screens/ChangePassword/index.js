import React, { Component } from 'react';
import { View, ImageBackground, Text, Image, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
import Button from "../../constants/button"
import { HeaderTitle } from 'react-navigation-stack';
import TextInputs from "../../constants/TextInput"
import styles from './style';
import Logo from "../../constants/Logo"
export default class ChangePassword extends Component {


    render() {
        return (
            <View>

                <ImageBackground source={require('../../assets/background.png')} style={styles.background}>
                    <Logo></Logo>
                       


                        <View style={styles.viewtxt}>
                            <Text style={styles.txt}>Change Password</Text>
                        </View>
                        <View>

                            <View style={styles.viewlock}>

                                <TextInputs
                                    placeHolder={"NEW PASSWORD"}
                                    uri={require("../../assets/lock.png")}
                                    image={styles.imglock}
                                ></TextInputs>
                            </View>
                            <View style={{ marginTop: "8%" }}>

                                <TextInputs
                                    placeHolder={"CONFIRM NEW PASSWORD"}
                                    uri={require("../../assets/lock.png")}
                                    image={styles.imglock}
                                ></TextInputs>
                            </View>
                                <View style={{ marginTop: 31}}>
                                    <Button title={"SUBMIT"} ></Button>
                                </View>
                        </View>
                </ImageBackground>
            </View>
        );
    }
}

