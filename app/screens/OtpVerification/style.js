import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({
    background:
    {
        width: "100%",
        height: "100%"
    },
    mpTxt: {
        color: "#fff",
        fontSize: 90,
        fontWeight: "bold",
        marginTop: "10%",
        fontStyle: "italic",
        textAlign: "center",
        letterSpacing: 3
    },
    line:
    {
        justifyContent: "center",
        marginTop: -22,
        marginLeft: "31%",
        width: wp("43%"),
        height: hp("0.7%")
    },
    txt:
    {
        color: "#fff",
        fontSize: 25,
        fontWeight: "bold",
        marginLeft: "30%",
        letterSpacing: 3
    },
    line2:
    {
        justifyContent: "center",
        marginTop: 1,
        marginLeft: "31%",
        width: wp("43%"),
        height: hp("0.7%")
    },
    frgttxt:
    {
        color: "white",
        fontWeight: "bold",
        fontSize: 18,
        textAlign: "center"
    },
    textotp:
    {
        color: "white",
        fontSize: 16,
        textAlign: "center",
        paddingLeft: "5%",
        paddingRight: "5%"
    },
    chngpsw:
    {
        color: "white",
        fontSize: 16,
        textAlign: "center",
        paddingLeft: "10%",
        paddingRight: "10%"
    },
    row:
    {
        flexDirection: "row",
        justifyContent: "center",
        alignSelf: "center",
        marginTop: "5%"
    },
    eline:
    {
        width: wp("35%"),
        height: hp("0.3%"),
        justifyContent: "center",
        alignSelf: "center",
        marginRight: "3%"
    },
    or:
    {
        color: "grey",
        textAlign: "center",
        justifyContent: "center",
        alignSelf: "center",
        fontSize: 20
    },
    eline2:
    {
        width: wp("35%"),
        height: hp("0.3%"),
        justifyContent: "center",
        alignSelf: "center",
        marginLeft: "3%"
    },
    placeholderview:
    {
        backgroundColor: "rgba(255, 255, 255, 0.41)",
        borderRadius: 50,




        ...Platform.select({
            ios: {
                height: hp("6%"),
 
        
            },
            android: {
                height: hp("7.5%"),

        
            }
        
          }),

        width: wp("90%"),
        marginLeft: "5%",
        marginRight: "5%",
        justifyContent:"center"
    },
    TextInputstyles: {
        height: 50,
        borderColor: '#fff',
        borderWidth: 1,
        width: '80%',
        borderRadius: 5,
        textAlign: 'center',
        color: '#fff'
    },
    textview:
    { borderBottomColor: "white",
     borderBottomWidth: 2, 
     width: "11%", marginBottom: 10, 
     color: "white", },
     textinputv:
     {

        ...Platform.select({
            ios: {
                top: 15, marginLeft: 13,


            },
            android: {
                top: 7, marginLeft: 10,


            }

        }),




        color: "white",
    },
    txtinner:
    {

        ...Platform.select({
            ios: {
                top: 15, marginLeft: 13,


            },
            android: {
                top: 7, marginLeft: 10,


            }

        }),




        color: "white",
    },
    txtin2view:
    { borderBottomColor: "white", borderBottomWidth: 2, width: "11%", marginBottom: 10, color: "white", },
    txtin2  :
     {
        color: "white",

        ...Platform.select({
            ios: {
                top: 15, marginLeft: 13,


            },
            android: {
                top: 7, marginLeft: 10,


            }

        }),

    },
});

export default styles;