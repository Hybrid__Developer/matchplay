import React, { Component } from 'react';
import { View, ImageBackground, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
import Button from "../../constants/button"
import TextInputs from "../../constants/TextInput"
import styles from './style';
import Logo from "../../constants/Logo"


export default class OtpVerification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            otp1: '',
            otp2: '',
            otp3: '',
            otp4: '',
        };
    }
    render() {
        return (
            <View>

                <ImageBackground source={require('../../assets/background.png')} style={styles.background}>

                    <Logo></Logo>
                    <View>
                        <View style={{ marginTop: "5%" }}>
                            <Text style={styles.frgttxt}>Enter OTP</Text>
                        </View>
                        <View style={{ marginTop: "3%" }}>
                            <Text style={styles.textotp}>Please enter OTP to change Your</Text>
                        </View>
                        <View style={{ marginTop: "1%" }}>
                            <Text style={styles.chngpsw}>password</Text>
                        </View>



                        <View style={[styles.placeholderview, { marginTop: 40, marginBottom: 40, flexDirection: "row", justifyContent: "space-evenly" }]}>

                            <View style={styles.textview}>
                                <TextInput
                                    style={styles.txtinner}
                                    maxLength={1}
                                    keyboardType='numeric'
                                    ref="input_1"
                                    onChangeText={otp1 => {
                                        this.setState({ otp1 })
                                        if (otp1) this.refs.input_2.focus();
                                    }}
                                />
                            </View>
                            <View style={styles.txtin2view}>

                                <TextInput
                                    style={styles.txtin2}
                                    maxLength={1}
                                    keyboardType='numeric'
                                    ref="input_2"
                                    onChangeText={otp2 => {
                                        this.setState({ otp2 })
                                        if (otp2) this.refs.input_3.focus();
                                    }}
                                />
                            </View>
                            <View style={styles.txtin2view}>

                                <TextInput
                                    style={styles.txtinner}
                                    maxLength={1}
                                    keyboardType='numeric'
                                    ref="input_3"
                                    onChangeText={otp3 => {
                                        this.setState({ otp3 })
                                        if (otp3) this.refs.input_4.focus();
                                    }}
                                />
                            </View>
                            <View style={styles.txtin2view}>

                                <TextInput
                                  style={styles.txtinner}
                                    maxLength={1}
                                    keyboardType='numeric'
                                    ref="input_4"
                                    onChangeText={otp4 => {
                                        this.setState({ otp4 })
                                        if (otp4) this.refs.input_4.focus();
                                    }}
                                />
                            </View>
                        </View>

                    </View>

                    <View style={{ marginTop: "5%" }}>
                        <Button title={"SUBMIT"} navigate={() => this.props.navigation.navigate("ChangePassword")}  ></Button>

                    </View>

                </ImageBackground>
            </View>
        );
    }
}

