import React, { Component } from 'react';
import { View, ImageBackground, Text, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';
import LinearGradient from 'react-native-linear-gradient';


import styles from './style';
export default class Splash extends Component {
  componentDidMount() {
    console.log("ssssss")
    this.timeoutHandle = setTimeout(() => {
      this.retrieveData();
    }, 2000);
  }
  retrieveData = () => {
    this.props.navigation.navigate("SignIn")
  }
  render() {
    return (
      <LinearGradient colors={['#34BA5B', '#2B6F23']}>
        <View  >
          <ScrollView>

            <ImageBackground style={styles.background} source={require('../../assets/bgblow.png')}>
              <View style={styles.textmpview}>
                <Text style={styles.textmp}>MP</Text>
                <Image

                  source={require('../../assets/Rectangle44.png')}
                  style={styles.line1}
                  resizeMode="cover"></Image>
                <View style={styles.text2view}>
                  <Text style={styles.text2}>MatchPlay!</Text>
                </View>
                <Image

                  source={require('../../assets/Rectangle44.png')}
                  style={styles.line2}
                  resizeMode="cover"></Image>

              </View>
            </ImageBackground>
          </ScrollView>

        </View>
      </LinearGradient>
    );
  }
}

