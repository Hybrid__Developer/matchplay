import { Dimensions, StyleSheet } from 'react-native';
const window = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../../utility/index';

const styles = StyleSheet.create({
    background:
    {
        width: wp("100%"),
        height: hp("100%")
    },
    textmpview:
    {
        marginTop: "68%",
        flexDirection: "column",
        justifyContent: "center",
        alignSelf: "center",
        marginLeft: "7%"
    },
    textmp:
    {
        color: "#618E2D",
        fontSize: 88,
        fontWeight: "bold",
        fontStyle: "italic",
        textAlign: "center",
        letterSpacing: 3,
        marginRight: "6%"
    },
    line1:
    {
        width: wp("35%"),
        height: hp("0.7%"),
        marginTop: "-5.5%",
        marginLeft:"0.5%"
    },
    text2:
    {
        color: "#618E2D",
        fontSize: 18,
        fontWeight: "bold",
        letterSpacing: 2,
    },
    line2:
    {
        width: wp("35%"),
        height: hp("0.8%"),
        marginTop: "-0%",
        marginLeft:"0.5%"
    },
    text2view:
        { marginTop: "-0.5%" }
});

export default styles;