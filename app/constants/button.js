import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet, } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../utility/index';
export default class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',

        }
    }
    submit = () => {
        this.props.submit?.();
    }
    render() {
        return (
            <View>
                <TouchableOpacity onPress={this.props.navigate}>
                    <LinearGradient colors={['#9c9e46', '#9c9e46', '#618E2D']} start={{ x: 0, y: 0.5 }} end={{ x: 1, y: 0.4 }} style={styles.btnview}>
                        {/* <View style={styles.btnmainview} > */}
                        <Text style={styles.btntext}>{this.props.title}</Text>
                        <Image

                            source={require('../assets/arrow.png')}
                            style={styles.image}
                            resizeMode="cover"></Image>
                        {/* </View> */}
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    btnview:
    {
        // marginTop: 40,
        borderRadius: 50,





        ...Platform.select({
            ios: {
                height: hp("6%"),
                width: wp("90%"),

            },
            android: {
                width: wp("90%"),
                height: hp("7.5%"),

            }

        }),
        alignSelf: "center",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center"

    },
    btnmainview:
    {
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center"
        // marginTop: "3%",
        // marginBottom: "3%"
    },
    btntext:
    {
        color: "white",
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center",
        justifyContent: "center",
        alignSelf: "center"
    },
    image:
    {

        ...Platform.select({
            ios: {
                width: wp("4%"),
                height: hp("2%"),

            },
            android: {
                width: wp("6%"),
                height: hp("3%"),

            }

        }),

        marginLeft: "5%",
        justifyContent: "center",
        alignSelf: "center"
    },

})