import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet, } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../utility/index';
export default class Logo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',

        }
    }
    submit = () => {
        this.props.submit?.();
    }
    render() {
        return (
            <View>
                <View style={{
                           ...Platform.select({
                            ios: {
                              marginTop: 50,
                
                            },
                            android: {
                              fontSize: 30,
                      
                            }
                      
                          }),
                        }}>
                        <Text style={styles.logotext}>MP</Text>



                        <Image

                            source={require('../assets/Rectangle.png')}
                            style={styles.line1}
                            resizeMode="cover"></Image>
                        <View >
                            <Text style={styles.textmp}>MatchPlay!</Text>
                        </View>
                        <Image

                            source={require('../assets/Rectangle.png')}
                            style={styles.line2}
                            resizeMode="cover">

                        </Image>
                        </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    logotext:
    {
        color: "#fff",
        fontSize: 90,
        fontWeight: "bold",
        marginTop: "10%",
        fontStyle: "italic",
        textAlign: "center",
        letterSpacing: 3
    },
    line1:
    {
        justifyContent: "center",
        marginLeft: "1%",
        width: wp("38%"),
        height: hp("0.5%"),
        alignSelf:"center",

        ...Platform.select({
            ios: {
                marginTop: -18,

        
            },
            android: {
                marginTop: -22,

        
            }
        
          }),


    },
    textmp:
    {
        color: "#fff",
        fontSize: 18,
        fontWeight: "bold",
        marginRight:"3%",
        textAlign:"center",
        letterSpacing: 3
    },
    line2:
    {
        justifyContent: "center",
        marginTop: 1,
        marginLeft: "1%",
        width: wp("38%"),
        height: hp("0.5%"),
        alignSelf:"center"
    },
   

})