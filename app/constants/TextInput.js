
import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet, TextInput, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '../utility/index';
export default class TextInputs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            placeHolder: '',

        }
    }

    render() {
        return (


            <View style={styles.placeholderview}>
                <View style={{ flexDirection: "row" }}>
                    <Image

                        source={this.props.uri}
                        style={this.props.image}
                        resizeMode="cover"></Image>
                    <TextInput
                        require
                        allowFontScaling={false}
                        placeholder={this.props.placeHolder}
                        placeholderTextColor="white"
                        keyboardType={this.props.keyboard}
                        style={{
                            color: "white",
                            fontSize: 17,
                            marginLeft: "6%",
                            fontWeight: "600"
                        }}
                    />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    placeholderview:
    {
        backgroundColor: "rgba(255, 255, 255, 0.41)",
        // marginTop: "15%",
        borderRadius: 50,

        ...Platform.select({
            ios: {
                height: hp("6%"),

            },
            android: {
                height: hp("7%"),

            }

        }),
        width: wp("90%"),
        marginLeft: "5%",
        marginRight: "5%",
        justifyContent: "center"
    },
    icon:
    {
        width: wp("7%"),
        height: hp("4%"),
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "flex-start",
        marginLeft: "5%"
    },
})