

import React from 'react';
import { Image, Text, ImageBackground, View } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
 import SplashScreen from '../screens/Splash';
import SignInScreen from '../screens/SignIn';
import SignUpScreen from '../screens/SignUp';
import ForgotPasswordScreen from '../screens/ForgotPassword';
import OtpVerificationScreen from '../screens/OtpVerification';
import HomeScreen from '../screens/Home';
import RequestScreen from '../screens/Request'
import PendingScreen from '../screens/Pending'
import FeedScreen from '../screens/Feed'
import AddPlayerScreen from '../screens/AddPlayer'
import BittingScreen from '../screens/Bitting'
import SearchPlayerScreen from '../screens/Request/SearchPlayer'

import ChangePasswordScreen from '../screens/ChangePassword';

const AppStack = createStackNavigator(
  {
        Splash: {
          screen: SplashScreen,
          navigationOptions: {
            headerShown: false,
          },
        },
       
        SignIn: {
          screen: SignInScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        SignUp: {
          screen: SignUpScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
    
        ForgotPassword: {
          screen: ForgotPasswordScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        ChangePassword: {
          screen: ChangePasswordScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        OtpVerification: {
          screen: OtpVerificationScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        Home: {
          screen: HomeScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        Request: {
          screen: RequestScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        SearchPlayer:{
          screen: SearchPlayerScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        Pending: {
          screen: PendingScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        Feed: {
          screen: FeedScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
        AddPlayer: {
          screen: AddPlayerScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
        },
       
        Bitting:{
          screen: BittingScreen,
          navigationOptions: {
            headerShown: false,
            visible: false
          },
          
        },
        
      },
      {
        
        initialRouteName: 'Splash',
      }
);



// bottom navigation
const TabNavigator2 = createBottomTabNavigator({
  Request: {
    screen: RequestScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "Request ",
      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "Request") {
          icon = focused ? require('../assets/Frame1.png') :
            require('../assets/Frame.png')

        }
        let txt
        if (navigation.state.routeName === "Request") {
          txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold" }}>REQUEST.M</Text> :
            <Text style={{ fontSize: 7, fontWeight: "bold" }}>REQUEST.M</Text>

        }
        return <View style={{ marginTop: 20, marginLeft: 10, marginBottom: 14, }}><Image
          source={icon}
          style={{
            width: 24,
            height: 24, marginLeft: 7
          }}></Image>

          <View>
            <Text >{txt}</Text></View>
        </View>
      }
    })
  },
  Pending: {
    screen: PendingScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "Pending ",
      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "Pending") {
          icon = focused ? require('../assets/flag1.png') :
            require('../assets/flagimg.png')

        }
        let txt
        if (navigation.state.routeName === "Pending") {
          txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold" }}>PENDING M.</Text> :
            <Text style={{ fontSize: 7, fontWeight: "bold" }}>PENDING M.</Text>

        }
        return <View style={{marginTop: 20, marginLeft: 10, marginBottom: 14}}><Image
          source={icon}
          style={{
            width: 26,
            height: 24, marginLeft: 10
          }}></Image>

          <View>
            <Text >{txt}</Text></View>
        </View>
      }
    })
  },
  AddPlayer: {
    screen:AddPlayerScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "ADD",

      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "AddPlayer") {
          icon = focused ? require('../assets/addbtn.png') :
            require('../assets/addbtn.png')
        }
      
        let txt
        if (navigation.state.routeName === "AddPlayer") {
          txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold", }}>ADD</Text> :
            <Text style={{ fontSize: 8, fontWeight: "bold",}}>ADD</Text>

        }
        return <View style={{marginTop: -5, marginBottom: 31, }}><Image
          source={icon}
          style={{
            width: 43,
            height: 43, 
          }}></Image>
          <Text style={{textAlign:"center",marginTop:"5%",top:3}}>{txt}</Text>
        </View>

      }
    })


  },
  
  
  Bitting: {
    screen: BittingScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "Reoprts",

      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "Bitting") {
          icon = focused ? require('../assets/poker1.png') :
            require('../assets/poker.png')
        }
        let txt
        if (navigation.state.routeName === "Bitting") {
          txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold", }}>BITTING</Text> :
            <Text style={{ fontSize: 7, fontWeight: "bold" ,}}>BITTING</Text>

        }
        return <View style={{ marginTop: 20, marginLeft: 5, marginBottom: 14,  }}><Image
          source={icon}
          style={{
            width: 24,
            height: 24, marginLeft: 5
          }}></Image>
          <Text>{txt}</Text>
        </View>

      }
    })


  },
  Feed: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: "Feed",

      tabBarIcon: ({ focused, tintColor }) => {
        let icon
        if (navigation.state.routeName === "Feed") {
          icon = focused ? require('../assets/report.png') :
            require('../assets/report.png')
        }
        let txt
        if (navigation.state.routeName === "Feed") {
          txt = focused ? <Text style={{ color: "green", fontSize: 7, fontWeight: "bold", }}>FEED</Text> :
            <Text style={{ fontSize: 7, fontWeight: "bold" ,}}>FEED</Text>

        }
        return <View style={{ marginTop: 25, marginLeft: 10, marginBottom: 14,  }}><Image
          source={icon}
          style={{
            width: 20,
            height: 20, marginLeft: 2,
          }}></Image>
          <Text>{txt}</Text>
        </View>

      }
    })


  },
},
 {
  initialRouteName: "Feed",
  tabBarPosition: 'bottom',
  swipeEnabled: true,
  tabBarOptions: {
    activeTintColor: 'white',
    activeBackgroundColor: '#FFFF',
    inactiveTintColor: 'white',
    inactiveBackgroundColor: '#FFF',
    showIcon: true,
    showLabel: true,
    style: { height: 54, borderTopColor: 'white', borderTopWidth: 1, elevation: 20 },
    labelStyle: {
      fontSize: 1,
      fontWeight: "bold",

    }
  }
}
);

const Routes = createAppContainer(
  createSwitchNavigator({
    App: AppStack,
    BottomTab2: TabNavigator2,
  }),
);
export default Routes;
